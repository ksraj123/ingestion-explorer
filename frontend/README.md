# Ingestion Explorer - Frontend

The frontend of the application is built using the **Vue** framework supported by **TypeScript** and
**SCSS**.

## Development

### Without Docker

The frontend only requires **node.js** and **npm** to be installed on the local machine. If the said
requirements are met, the non-docker solution is simpler.

```bash
cd explorer
npm ci
npm run serve
```

Visit `http://localhost:7001` on the browser.

### With Docker

#### Build Docker Image

```bash
docker build -t ingestor-ingestion-explorer-frontend:dev -f development.dockerfile .
```

#### Run Docker Container

##### Using Docker Compose

For development, prefer `docker-compose` as everything is cofigured in the `docker-compose.yml` file
and the command is much shorter and simpler.

```bash
docker-compose up -d
```

In another terminal, run

```bash
docker attach ingestor-ingestion-explorer-frontend
```

To stop, run (in the terminal where you entered `docker-compose up -d`)

```bash
docker-compose down
```

##### Using Docker Run

```bash
docker run --mount type=bind,src="$(pwd)/explorer",dst=/app -p 7001:8080 --name ingestor-ingestion-explorer-frontend -it --rm ingestor-ingestion-explorer-frontend:dev
```

#### Run the Server

```bash
npm ci
npm run serve
```

Visit `http://localhost:7001` on the browser.

## Production

### Build Docker Image

```bash
docker build -t ingestor-ingestion-explorer-frontend:latest -f production.dockerfile .
```

### Run Docker Container

```bash
docker run --rm -p 7001:80 --name ingestor-ingestion-explorer-frontend ingestor-ingestion-explorer-frontend:latest
```

## Application Structure

The source code of the Frontend application is present in the `explorer` directory.

The `src` subdirectory is the root directory of the application source.

- `@types` - contains some _TypeScript_ type/interface definitions.
- `components` - contains the _Vue_ components.
- `lib` - contains locally loaded library files.
- `models` - contains some utility classes.
- `scss` - contains some _SCSS_ utilities.
- `utils` - contains some utility functions.

- `App.vue` - the root _Vue_ component.

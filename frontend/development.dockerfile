# base from official Node (Alpine LTS) image
FROM node:lts-alpine

# install simple http server for serving static content
RUN npm install -g http-server

# set /app as working directory (in development mode for mounting source code)
WORKDIR /app

# override default CMD for image ("node"): launch the shell
CMD sh

# Open ports
EXPOSE 8080

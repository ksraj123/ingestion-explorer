###############
#   Stage 1   #
###############

# Base from official Node (Alpine-LTS) image for base image
FROM node:lts-alpine as base

# set /app as working directory (for mounting source code)
WORKDIR /app

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY ./explorer ./

# install build tools
RUN apk add --no-cache --virtual .gyp python3 make g++

# install dependencies and build app for production with minification
RUN npm ci && npm run build


###############
#   Stage 2   #
###############

# Base from latest official Alpine image for final image
FROM alpine:latest as final

# Install thttpd
RUN apk add thttpd

# Create a non-root user to own the files and run our server
RUN adduser -D static
USER static
WORKDIR /home/static

# Copy built static files from base stage
COPY --from=base /app/dist .

# Run thttpd
CMD ["thttpd", "-D", "-h", "0.0.0.0", "-p", "80", "-d", "/home/static", "-u", "static", "-l", "-", "-M", "60"]

# Open ports
EXPOSE 80

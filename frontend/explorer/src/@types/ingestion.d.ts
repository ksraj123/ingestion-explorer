/** Represents the package for the source transform endpoint. */
export interface IPackageTransformerSource {
    source_type: string;
    origin_type: string;
    source: string;
    transformer: string;
    arguments: { [key: string]: string };
}

/** Represents an Ingestum document. */
export interface ITransformerDocument {
    type: string;
    title: string = '';
    content: string;
    context: { [key: string]: string };
    origin: any;
    version: string = '1.0';
    pdf_context: any;
}

/** Represents the package for the document transform endpoint. */
export interface IPackageTransformerDocument {
    document: ITransformerDocument;
    transformer: string;
    arguments: { [key: string]: string };
}

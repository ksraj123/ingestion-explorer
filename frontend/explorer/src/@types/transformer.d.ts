import { TSource } from './source';

export type TTransformerArgType = 'String' | 'Integer';

export interface ITransformerInput {
    name: string;
    description: string;
    inputs: {
        [key: string]: string;
    };
    outputs: {
        [key: string]: string;
    };
    arguments: {
        [key: string]: string;
    } | null;
}

export interface ITransformer {
    name: string;
    description: string;
    inputFamily: string;
    inputType: TSource;
    outputFamily: string;
    outputType: TSource;
    takesArgs: boolean;
    argKeys: string[];
    getArgType(arg: string): TTransformerArgType | null;
}

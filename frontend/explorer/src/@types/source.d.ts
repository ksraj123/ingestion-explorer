export type TSourceFile =
    | 'Audio'
    | 'CSV'
    | 'HTML'
    | 'Image'
    | 'PDF'
    | 'Text'
    | 'XLS'
    | 'XML'
    | 'DOC';

export type TSourceNonFile = 'Email' | 'Proquest' | 'Pubmed' | 'Twitter';

export type TSource = TSourceFile | TSourceNonFile;

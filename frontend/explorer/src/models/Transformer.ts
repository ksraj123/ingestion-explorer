import { ITransformer, ITransformerInput, TTransformerArgType } from '../@types/transformer';
import { TSource } from '../@types/source';

export default class Transformer implements ITransformer {
    private _name: string;
    private _description: string;
    private _inputs: {
        [key: string]: TSource;
    };
    private _outputs: {
        [key: string]: TSource;
    };
    private _args: { [key: string]: TTransformerArgType } | null;

    private _argKeys: string[];

    constructor(params: ITransformerInput) {
        this._name = params.name;
        this._description = params.description;
        this._inputs = params.inputs as { [key: string]: TSource };
        this._outputs = params.outputs as { [key: string]: TSource };
        this._args = params.arguments as { [key: string]: TTransformerArgType };

        this._argKeys = this._args !== null ? Object.keys(this._args) : [];
    }

    public get name(): string {
        return this._name;
    }

    public get description(): string {
        return this._description;
    }

    public get inputFamily(): string {
        return Object.keys(this._inputs)[0];
    }

    public get inputType(): TSource {
        return this._inputs[this.inputFamily];
    }

    public get outputFamily(): string {
        return Object.keys(this._outputs)[0];
    }

    public get outputType(): TSource {
        return this._outputs[this.outputFamily];
    }

    public get takesArgs(): boolean {
        return this._args !== null;
    }

    public get argKeys(): string[] {
        return this._argKeys;
    }

    public getArgType(arg: string): TTransformerArgType | null {
        return this._args !== null ? (arg in this._args ? this._args[arg] : null) : null;
    }
}

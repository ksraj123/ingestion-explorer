import dotenv from 'dotenv';
dotenv.config();

import { createApp } from 'vue';
import App from './App.vue';
import { Auth0Client } from '@auth0/auth0-spa-js';

(async () => {
    const app = createApp(App);

    const auth0Client = new Auth0Client({
        domain: process.env.VUE_APP_AUTH0_DOMAIN as string,
        client_id: process.env.VUE_APP_AUTH0_CLIENT_ID as string,
        redirect_uri: (process.env.NODE_ENV === 'production'
            ? process.env.VUE_APP_PROD_URL
            : process.env.VUE_APP_DEV_URL) as string
    });
    app.config.globalProperties.$auth = auth0Client;

    app.mount('#app');
})();

import {
    IPackageTransformerDocument,
    IPackageTransformerSource,
    ITransformerDocument
} from '../@types/ingestion';

async function transform(
    type: 'source' | 'document',
    transformerPackage: IPackageTransformerSource | IPackageTransformerDocument,
    token: string
): Promise<ITransformerDocument> {
    const response = await (async () => {
        const apiURL =
            process.env.NODE_ENV === 'production'
                ? process.env.VUE_APP_API_URL
                : process.env.VUE_APP_DEV_API_URL;
        const response = await fetch(`${apiURL}/ingestion/transform-${type}`, {
            method: 'POST',
            body: JSON.stringify({ ...transformerPackage }),
            headers: new Headers({
                'Access-Control-Allow-Origin': '*',
                'api_key': token
            })
        });
        return response.json();
    })();
    return new Promise<ITransformerDocument>((resolve) => resolve(response));
}

export async function transformSource(
    token: string,
    transformerPackage: IPackageTransformerSource
): Promise<ITransformerDocument> {
    const response = await transform('source', transformerPackage, token);
    return new Promise<ITransformerDocument>((resolve) => resolve(response));
}

export async function transformDocument(
    token: string,
    transformerPackage: IPackageTransformerDocument
): Promise<ITransformerDocument> {
    const response = await transform('document', transformerPackage, token);
    return new Promise<ITransformerDocument>((resolve) => resolve(response));
}

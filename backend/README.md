# Ingestion Explorer - Backend

The backend runs a _Python_ **FastAPI** server that exposes a _REST API_ for the frontend application
to interact with. The server used is **uvicorn** which is an _ASGI Web Server_ implementation for
_Python_. **Gunicorn**, which is a _Python WSGI HTTP Server_ for _UNIX_, is used to run and manage
**Uvicorn** in a production setting. **Uvicorn** includes a **gunicorn** _worker class_ that means
you can get set up with very little configuration.

## Development

This setup requires access to the public URL of the server running this application. For development,
setup [ngrok](https://ngrok.com/). Start a tunnel to the server serving this application on your
local machine. The public URL generated must be added as an environment variable. In production, it
will be replaced with the public URL of the deployment.

Say, the backend server is attached to port `7000`, so the _ngrok_ command is:

```bash
ngrok http 7000
```

### Without Docker

#### Setup

For development using the _Python Virtual Environment_ over _Docker_ is actually simpler.

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements-dev.txt
```

#### Run the Server

Replace the value of the `INGESTOR_APP_PUBLIC_URL` environment variable with your _ngrok_ tunnel URL.

```bash
export INGESTOR_APP_PUBLIC_URL=https://123456.ngrok.io/
```

To disable requests authentication (for debugging)

```bash
export INGESTOR_DISABLE_AUTH=1
```

Start the server using

```bash
uvicorn --host 0.0.0.0 --port 7000 --reload explorer.main:app
```

### With Docker

#### Build Docker Image

```bash
DOCKER_BUILDKIT=1 docker build --secret id=token,src=$HOME/.git-credentials -t ingestor-ingestion-explorer-backend:dev -f development.dockerfile .
```

#### Run Docker Container

##### Using Docker Compose

For development, prefer `docker-compose` as everything is cofigured in the `docker-compose.yml` file
and the command is much shorter and simpler.

```bash
docker-compose up -d
```

In another terminal, run

```bash
docker attach ingestor-ingestion-explorer-backend
```

To stop, run (in the terminal where you entered `docker-compose up -d`)

```bash
docker-compose down
```

##### Using Docker Run

```bash
docker run --mount type=bind,src="$(pwd)"/env/,dst=/app/env/ --mount type=bind,src="$(pwd)"/explorer/,dst=/app/app/ --mount type=bind,src="$(pwd)"/workspace/,dst=/app/workspace/ -p 7000:7000 --env-file ./env/config.env --name ingestor-ingestion-explorer-backend -it --rm ingestor-ingestion-explorer-backend:dev
```

#### Run the Server

Replace the value of the `INGESTOR_APP_PUBLIC_URL` environment variable with your _ngrok_ tunnel URL.

```bash
export INGESTOR_APP_PUBLIC_URL=https://123456.ngrok.io/
```

To disable requests authentication (for debugging)

```bash
export INGESTOR_DISABLE_AUTH=1
```

Start the server using

```bash
uvicorn --host 0.0.0.0 --port 7000 --reload explorer.main:app
```

## Production

### Build Docker Image

```bash
DOCKER_BUILDKIT=1 docker build --secret id=token,src=$HOME/.git-credentials -t ingestor-ingestion-explorer-backend:latest -f production.dockerfile .
```

### Run Docker Container

```bash
docker run --rm -p 7000:80 -e INGESTOR_APP_PUBLIC_URL=https://backend.ingestion-explorer.gcp-cicd.ingestor.io/ --name ingestor-ingestion-explorer-backend ingestor-ingestion-explorer-backend:latest
```

## Application Structure

The source code of the Backend application is present in the `explorer` directory.

- `main.py` - Main module of the application
- `config.py` - Creates the application configurations from environment
- `transformers.json` - Contains the data structure containing the list and metadata of transformers

The REST API routers are present in the corresponding `explorer/api/vX` subdirectory.

- `api.py` - Root router module for `/api/vX/`
- `endpoints` subdirectory contains the other routers under `/api/vX/`

  For `/api/v1`:

  - `models.py` - Contains pydantic `BaseModel` subclasses used by `Explorer` in `explorer.py`
  - `files.py` - Contains routers for file operations
  - `ingestion.py` - Contains routers for ingestion

The `workspace` directory contains the static files used as sources by the ingestion service.

The `workspace/samples` subdirectory contains sample source files for test use.

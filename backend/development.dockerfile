# syntax=docker/dockerfile:1

###############
#   Stage 1   #
###############

# Base from official Python 3.8 (Alpine 3.13) image for base image
FROM python:3.8-alpine3.13 as base

RUN apk update

# Install command-line tools
RUN apk add --no-cache python3-dev git gcc

# Make sure to use store credentials (.git-credentials)
RUN git config --global credential.helper store

# Install python dependencies
COPY requirements.txt .
RUN --mount=type=secret,id=token,dst=/root/.git-credentials \
    pip3 install -r requirements.txt


###############
#   Stage 2   #
###############

# Base from Gunicorn/Uvicorn on Python 3.8 (Alpine 3.10) image for final image
FROM tiangolo/uvicorn-gunicorn:python3.8-alpine3.10

# Install certificates for SSL/TLS
RUN apk --no-cache add ca-certificates

# Copy installed python packages and headers from base stage
COPY --from=base /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages
COPY --from=base /usr/local/include/python3.8 /usr/local/include/python3.8

# Add logger configuration file
COPY logging.conf /opt/

# Override default CMD for image ("/start.sh"): launch the shell
CMD sh

# Open ports
EXPOSE 7000

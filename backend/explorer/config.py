from functools import lru_cache
from pydantic import BaseSettings


class Settings(BaseSettings):
    """Creates configuration global fields."""

    INGESTOR_APP_NAME: str = "Ingestion Explorer"
    INGESTOR_API_TREE: str = "/api/v1"

    # from configuration environment file
    INGESTOR_WORKSPACE_DIR: str = "workspace"
    INGESTOR_APP_DIST_DIR: str = "dist"
    INGESTOR_APP_PUBLIC_URL: str
    INGESTOR_AUTH_HOST: str
    INGESTOR_AUTH_HOST_CLIENT_ID: str
    INGESTOR_INGESTION_HOST: str
    INGESTOR_INGESTION_HOST_API_KEY: str

    # configure .env file source
    class Config:
        env_file = "./env/config.env"


@lru_cache()
def get_settings():
    return Settings()

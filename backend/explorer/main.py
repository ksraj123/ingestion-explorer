# import configparser
# import logging
import os
import pathlib
from urllib.parse import urljoin

from fastapi.middleware.cors import CORSMiddleware
from ingestor_ingestion_utils.server import BaseServer
from starlette.responses import FileResponse

from .config import get_settings

from .middleware import SorceroAuthMiddleware


# fetch configuration settings
settings = get_settings()

# conditionally import the configured API version's router
if settings.INGESTOR_API_TREE.split("/")[-1] == "v1":
    from .api.v1.api import api_router


base_dir = os.path.dirname(os.path.dirname(__file__))


class App(BaseServer):
    """FastAPI server application based on `server` module of `ingestor-ingestion-utils`."""

    def __init__(self):
        super().__init__(
            title=settings.INGESTOR_APP_NAME,
            openapi_url=urljoin(settings.INGESTOR_API_TREE, "openapi.json"),
        )

        self.authClientId = settings.INGESTOR_AUTH_HOST_CLIENT_ID

        # ensure workspace and dist directory exists
        pathlib.Path(settings.INGESTOR_WORKSPACE_DIR).mkdir(parents=True, exist_ok=True)
        pathlib.Path(settings.INGESTOR_APP_DIST_DIR).mkdir(parents=True, exist_ok=True)

        if "INGESTOR_DISABLE_AUTH" in os.environ:
            return

        origins = ["*"]
        self.add_middleware(
            CORSMiddleware,
            allow_origins=origins,
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )

        # add middleware to validate api_key in requests
        self.add_middleware(SorceroAuthMiddleware)


# instantiate the FastAPI application
app = App()

# include router for the configured API version
app.include_router(api_router, prefix=settings.INGESTOR_API_TREE)


@app.get("/", include_in_schema=False)
async def index():
    dist_dir = os.path.join(base_dir, settings.INGESTOR_APP_DIST_DIR)
    if "index.html" in [
        f for f in os.listdir(dist_dir) if os.path.isfile(os.path.join(dist_dir, f))
    ]:
        return FileResponse(
            os.path.join(dist_dir, "index.html"),
            headers={"Cache-Control": "no-cache"},
        )
    else:
        return {"msg": "Ingestion Explorer"}

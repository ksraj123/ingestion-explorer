from starlette.datastructures import Headers
from starlette.types import ASGIApp, Receive, Scope, Send
from starlette.responses import JSONResponse

import secrets

from ingestor_jwt import ingestor_jwt as auth_manager


class SorceroAuthMiddleware:
    def __init__(self, app: ASGIApp) -> None:
        self.app = app

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        if scope.get("method") == "OPTIONS":
            await self.app(scope, receive, send)
            return

        headers = Headers(scope=scope)
        api_key = headers.get("api_key", "")

        try:
            if api_key:
                pass
            else:
                raise PermissionError
        except Exception as e:
            deny = JSONResponse({"status": "denied"}, status_code=401)
            await deny(scope, receive, send)
            return

        await self.app(scope, receive, send)

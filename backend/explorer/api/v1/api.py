from fastapi import APIRouter

from ...config import get_settings
from .endpoints import files, ingestion

# fetch configuration settings
settings = get_settings()

# root router
api_router = APIRouter()


@api_router.get("/", tags=["root"])
def root():
    """
    Dummy root.
    """

    return {"message": f"Hello World from {settings.INGESTOR_APP_NAME}"}


# include router handling file operations
api_router.include_router(files.router, prefix="/files", tags=["files"])
# include router handling ingestion
api_router.include_router(ingestion.router, prefix="/ingestion", tags=["ingestion"])

from typing import Dict


def format_base_document(schema: Dict) -> Dict:
    """
    Filters out the BaseDocument model from the original schema.

    Arguments:
        Original OpenAPI schema

    Returns:
        A formatted dictionary representing the BaseDocument model
    """

    return {
        k: v.get("type")
        for k, v in schema.get("BaseDocument").get("properties").items()
    }


def format_content(schema: Dict) -> Dict:
    """
    Filters out the Content model from the original schema.

    Arguments:
        Original OpenAPI schema

    Returns:
        A formatted dictionary representing the Content model
    """

    new_dict = {
        k: {_k: _v for _k, _v in v.items() if _k != "title"}
        for k, v in schema.get("Content").get("properties").items()
    }
    for k, v in new_dict.items():
        if k not in schema.get("Content").get("required"):
            v["optional"] = True

    return new_dict


def format_crop_area(schema: Dict) -> Dict:
    """
    Filters out the CropArea model from the original schema.

    Arguments:
        Original OpenAPI schema

    Returns:
        A formatted dictionary representing the CropArea model
    """

    new_dict = {
        k: {_k: _v for _k, _v in v.items() if _k != "title"}
        for k, v in schema.get("CropArea").get("properties").items()
    }
    for k, v in new_dict.items():
        if k not in schema.get("CropArea").get("required"):
            v["optional"] = True

    return new_dict


def format_metadata(schema) -> Dict:
    """
    Filters out the Metadata model from the original schema.

    Arguments:
        Original OpenAPI schema

    Returns:
        A formatted dictionary representing the Metadata model
    """

    new_dict = {
        k: {_k: _v for _k, _v in v.items() if _k != "title"}
        for k, v in schema.get("Metadata").get("properties").items()
    }
    for k, v in new_dict.items():
        if k not in schema.get("Metadata").get("required"):
            v["optional"] = True

    return new_dict


def format_pdf_context(schema) -> Dict:
    """
    Filters out the PDFContext model from the original schema.

    Arguments:
        Original OpenAPI schema

    Returns:
        A formatted dictionary representing the PDFContext model
    """

    new_dict = {
        k: {_k: _v for _k, _v in v.items() if _k != "title"}
        for k, v in schema.get("PDFContext").get("properties").items()
    }
    for k, v in new_dict.items():
        if k not in schema.get("PDFContext").get("required"):
            v["optional"] = True

    return new_dict


def filter_ingestum(schema: Dict) -> Dict:
    """
    Filters out the Ingestum specific keys from the original schema.

    Arguments:
        Original OpenAPI schema

    Returns:
        A filtered dictionary containing only Ingestum specific keys
    """

    return {k: v for k, v in schema.items() if k.startswith("ingestum__")}


def filter_sources(schema: Dict) -> Dict:
    """
    Filters out the Ingestum sources keys from the original schema.

    Arguments:
        Ingestum filtered schema

    Returns:
        A filtered dictionary containing only Ingestum sources keys
    """

    sources = {k: v for k, v in schema.items() if k.startswith("ingestum__sources__")}

    filtered_dict = {}
    offset_front = len("ingestum__sources__")
    offset_rear = len("__Source")

    for k, v in sources.items():
        newkey = k[offset_front:-offset_rear]

        filtered_dict[newkey] = {
            _k: _v.get("type")
            for _k, _v in v.get("properties").items()
            if _k not in ["type", "path"]
        }
        if len(filtered_dict[newkey].keys()) == 0:
            filtered_dict[newkey] = None

    return filtered_dict


def filter_documents(schema: Dict) -> Dict:
    """
    Filters out the Ingestum documents keys from the original schema.

    Arguments:
        Ingestum filtered schema

    Returns:
        A filtered dictionary containing only Ingestum documents keys
    """

    documents = {
        k: v for k, v in schema.items() if k.startswith("ingestum__documents__")
    }
    filtered_dict = {}
    offset_front = len("ingestum__documents__")
    offset_rear = len("__Document")

    filtered_dict = {
        k[offset_front:-offset_rear]: v.get("properties") for k, v in documents.items()
    }

    for k, v in filtered_dict.items():
        for _k, _v in v.items():
            if "$ref" in _v:
                filtered_dict[k][_k] = _v.get("$ref").split("/")[3]
            elif "type" in _v:
                filtered_dict[k][_k] = _v.get("type")

    return filtered_dict


def filter_manifest_sources(schema: Dict) -> Dict:
    """
    Filters out the Ingestum manifest sources keys from the original schema.

    Arguments:
        Ingestum filtered schema

    Returns:
        A filtered dictionary containing only Ingestum manifest sources keys
    """

    manifest_sources = {
        k: v
        for k, v in schema.items()
        if k.startswith("ingestum__manifests__sources__")
    }

    filtered_dict = {}
    offset_front = len("ingestum__manifests__sources__")
    offset_rear = len("__Source")

    for k, v in manifest_sources.items():
        filtered_properties = {
            _k: _v.get("type")
            for _k, _v in v.get("properties").items()
            if _k not in ["type", "id", "pipeline", "url", "credential"]
            and not _k.endswith("_placeholder")
        }
        filtered_dict[k[offset_front:-offset_rear]] = {
            "required": [
                i for i in v.get("required") if i not in ["id", "pipeline", "url"]
            ],
            "properties": filtered_properties,
        }

    for k, v in filtered_dict.items():
        if len(v.get("properties").keys()) == 0:
            filtered_dict[k] = None
            continue

        new_dict = {
            _k: {"type": _v, "required": False}
            for _k, _v in v.get("properties").items()
        }
        for i in v.get("required"):
            new_dict[i]["required"] = True
        filtered_dict[k] = new_dict

    return filtered_dict


def filter_conditionals(schema: Dict) -> Dict:
    """
    Filters out the Ingestum conditionals keys from the original schema.

    Arguments:
        Ingestum filtered schema

    Returns:
        A filtered dictionary containing only Ingestum conditionals keys
    """

    conditionals = {
        k: v for k, v in schema.items() if k.startswith("ingestum__conditionals__")
    }

    filtered_dict = {}

    offset_front = len("ingestum__conditionals__")
    offset_rear = len("__Conditional")

    for k, v in conditionals.items():
        if k.endswith("__Conditional"):
            inputs_model = conditionals.get(f"{k}__InputsModel")
            arguments_model = conditionals.get(f"{k}__ArgumentsModel")
            if inputs_model == None or arguments_model == None:
                continue

            newkey = k[offset_front:-offset_rear]

            filtered_dict[newkey] = {
                "description": v.get("description"),
                "inputs": {
                    "required": inputs_model.get("required"),
                    "properties": inputs_model.get("properties"),
                },
                "arguments": {
                    "required": arguments_model.get("required"),
                    "properties": arguments_model.get("properties"),
                },
            }

            for _k, _v in (
                filtered_dict.get(newkey).get("arguments").get("properties").items()
            ):
                if "anyOf" in _v:
                    filtered_dict[newkey]["arguments"]["properties"][_k] = [
                        i.get("$ref").split("/")[3].split("__")[-2]
                        for i in _v.get("anyOf")
                    ]
                elif "type" in _v:
                    filtered_dict[newkey]["arguments"]["properties"][_k] = _v.get(
                        "type"
                    )

            for _k, _v in (
                filtered_dict.get(newkey).get("inputs").get("properties").items()
            ):
                if "$ref" in _v:
                    ref_value = _v.get("$ref").split("/")[3]
                    filtered_dict[newkey]["inputs"]["properties"][_k] = (
                        ref_value.split("__")[-2]
                        if len(ref_value.split("__")) > 1
                        else ref_value
                    )
                if "title" in _v:
                    del filtered_dict[newkey]["inputs"]["properties"][_k]["title"]

            if len(filtered_dict[newkey]["inputs"].get("properties").keys()) == 0:
                filtered_dict[newkey]["inputs"] = None

    return filtered_dict


def filter_transformers(schema: Dict) -> Dict:
    """
    Filters out the Ingestum transformers keys from the original schema.

    Arguments:
        Ingestum filtered schema

    Returns:
        A filtered dictionary containing only Ingestum transformers keys
    """

    transformers = {
        k: v for k, v in schema.items() if k.startswith("ingestum__transformers__")
    }

    filtered_dict = {}
    offset_front = len("ingestum__transformers__")
    offset_rear = len("__Transformer")

    for k, v in transformers.items():
        if k.endswith("__Transformer"):
            inputs_model = transformers.get(f"{k}__InputsModel")
            outputs_model = transformers.get(f"{k}__OutputsModel")
            arguments_model = transformers.get(f"{k}__ArgumentsModel")
            if inputs_model == None or outputs_model == None or arguments_model == None:
                continue

            newkey = k[offset_front:-offset_rear]

            filtered_dict[newkey] = {
                "description": v.get("description"),
                "inputs": {
                    "required": inputs_model.get("required"),
                    "properties": {
                        _k: (
                            _v.get("$ref").split("/")[3].split("__")[-2]
                            if len(_v.get("$ref").split("/")[3].split("__")) > 1
                            else _v.get("$ref").split("/")[3]
                        )
                        for _k, _v in inputs_model.get("properties").items()
                    },
                },
                "outputs": {
                    "required": outputs_model.get("required"),
                    "properties": {
                        _k: _v for _k, _v in outputs_model.get("properties").items()
                    },
                },
                "arguments": {
                    "properties": {
                        _k: _v for _k, _v in arguments_model.get("properties").items()
                    }
                },
            }

            for _k, _v in (
                filtered_dict[newkey].get("outputs").get("properties").items()
            ):
                if "$ref" in _v:
                    filtered_dict[newkey]["outputs"]["properties"][_k] = (
                        _v.get("$ref").split("/")[3].split("__")[-2]
                        if len(_v.get("$ref").split("/")[3].split("__")) > 1
                        else _v.get("$ref").split("/")[3]
                    )
                elif "anyOf" in _v:
                    filtered_dict[newkey]["outputs"]["properties"][_k] = [
                        i.get("$ref").split("/")[3].split("__")[-2]
                        if len(i.get("$ref").split("/")[3].split("__")) > 1
                        else i.get("$ref").split("/")[3]
                        for i in _v.get("anyOf")
                    ]

            for _k, _v in (
                filtered_dict[newkey].get("arguments").get("properties").items()
            ):
                if "$ref" in _v:
                    filtered_dict[newkey]["arguments"]["properties"][_k] = _v.get(
                        "$ref"
                    ).split("/")[3]
                elif "type" in _v:
                    filtered_dict[newkey]["arguments"]["properties"][_k] = _v.get(
                        "type"
                    )

            if len(filtered_dict[newkey]["arguments"].get("properties").keys()) == 0:
                filtered_dict[newkey]["arguments"] = None
            else:
                if "required" in arguments_model.keys():
                    filtered_dict[newkey]["arguments"][
                        "required"
                    ] = arguments_model.get("required")
                else:
                    filtered_dict[newkey]["arguments"]["required"] = []

    for k, v in filtered_dict.items():
        new_inputs_dict = {
            _k: {"type": _v} for _k, _v in v.get("inputs").get("properties").items()
        }
        for _k, _v in v.get("inputs").get("properties").items():
            if _k not in v.get("inputs").get("required"):
                new_inputs_dict[_k]["optional"] = True
        filtered_dict[k]["inputs"] = new_inputs_dict

        new_outputs_dict = {
            _k: {"type": _v} for _k, _v in v.get("outputs").get("properties").items()
        }
        for _k, _v in v.get("outputs").get("properties").items():
            if _k not in v.get("outputs").get("required"):
                print(k, _k)
                new_outputs_dict[_k]["optional"] = True
        filtered_dict[k]["outputs"] = new_outputs_dict

        if v.get("arguments") is not None:
            new_arguments_dict = {
                _k: {"type": _v}
                for _k, _v in v.get("arguments").get("properties").items()
            }
            for _k, _v in v.get("arguments").get("properties").items():
                if _k not in v.get("arguments").get("required"):
                    new_arguments_dict[_k]["optional"] = True
            filtered_dict[k]["arguments"] = new_arguments_dict

            for _k, _v in filtered_dict.get(k).get("arguments").items():
                if type(_v.get("type")) is dict:
                    if "anyOf" in _v.get("type"):
                        if _v.get("type").get("title") == "Transformer":
                            offset_front = len("ingestum__transformers__")
                            offset_rear = len("__Transformer")

                            _v["type"]["anyOf"] = [
                                i.get("$ref").split("/")[3][offset_front:-offset_rear]
                                for i in _v.get("type").get("anyOf")
                            ]
                        elif _v.get("type").get("title") == "Conditional":
                            offset_front = len("ingestum__conditionals__")
                            offset_rear = len("__Conditional")

                            _v["type"]["anyOf"] = [
                                i.get("$ref").split("/")[3][offset_front:-offset_rear]
                                for i in _v.get("type").get("anyOf")
                            ]

    return filtered_dict


def format_transformer(
    name: str, transformer: Dict, compress_type_object: bool = False
) -> Dict:
    """
    Converts a transformer name, transformer dictionary to combined dictionary.

    Arguments:
        name: transformer name
        transformer: dictionary containing the details of the transformer
        compress_type_object: whether to convert the inputs and outputs element's type object to
            string

    Returns:
        Transformer details dictionary with added key name
    """

    new_dict = {"name": name}
    for k, v in transformer.items():
        new_dict[k] = (
            {_k: _v.get("type") for _k, _v in v.items()}
            if (compress_type_object and k in ["inputs", "outputs"])
            else v
        )
    return new_dict


def format_transformers(transformers: Dict) -> Dict:
    """
    Separates the source transformers and the document transformers.

    Arguments:
        Ingestum transformers schema

    Returns:
        A formatted dictionary separting Ingestum source transformers and Ingestum document
        transformers
    """

    new_dict = {"fromSource": {}, "fromDocument": {}, "fromMultiple": []}
    for k, v in transformers.items():
        if len(v.get("inputs").keys()) == 1:
            key = [i for i in v.get("inputs").keys()][0]
            dictkey = "fromSource" if key == "source" else "fromDocument"
            input_type = v.get("inputs").get(key).get("type")
            if input_type in new_dict.get(dictkey):
                new_dict[dictkey][input_type].append(format_transformer(k, v, True))
            else:
                new_dict[dictkey][input_type] = [format_transformer(k, v, True)]
        else:
            new_dict["fromMultiple"].append(format_transformer(k, v))
    return new_dict


def format_schema(schema: Dict) -> Dict[str, Dict]:
    """
    Assembles all bits of the formatted schema.

    Arguments:
        Original OpenAPI schema

    Returns:
        A dictionary representing the formatted schema
    """

    ingestum_schema = filter_ingestum(schema)
    return {
        "BaseDocument": format_base_document(schema),
        "CropArea": format_crop_area(schema),
        "Content": format_content(schema),
        "Metadata": format_metadata(schema),
        "PDFContext": format_pdf_context(schema),
        "sources": filter_sources(ingestum_schema),
        "documents": filter_documents(ingestum_schema),
        "manifest_sources": filter_manifest_sources(ingestum_schema),
        "conditionals": filter_conditionals(ingestum_schema),
        "transformers": format_transformers(filter_transformers(ingestum_schema)),
    }

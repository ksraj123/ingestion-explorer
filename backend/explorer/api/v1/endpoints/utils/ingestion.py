import hashlib
import json
import os
from typing import Dict
from urllib.parse import urljoin

from fastapi.encoders import jsonable_encoder
import requests_async as requests

from .....config import get_settings
from ..models.ingestion import EnvelopeModel, ManifestModel, PipelineModel


# fetch configuration settings
settings = get_settings()


async def fetch_schema() -> Dict:
    """
    Fetches the OpenAPI.json schema from the ingestion host.

    Arguments:
        None

    Returns:
        Schema GET response
    """

    headers = {"api_key": settings.INGESTOR_INGESTION_HOST_API_KEY}

    url = urljoin(settings.INGESTOR_INGESTION_HOST, "/openapi.json")
    response = await requests.get(url, headers=headers)

    return response


def make_file(path: str, content: str) -> None:
    """
    Creates a local file in the workspace directory.

    Arguments:
        path: path to the file
        content: content to dump in the file

    Returns:
        None
    """

    with open(path, "w+") as filehandle:
        filehandle.write(content)


def remove_file(path: str) -> None:
    """
    Removes a local file in the workspace directory

    Arguments:
        path: path to the file

    Returns:
        None
    """

    os.remove(path)


def structure_manifest(type: str, url: str, api_key: str = None) -> ManifestModel:
    """
    Packs content according to the ManifestModel as required by the ingestion service.

    Arguments:
        type: type of source
        url: public URL of source
        api_key: authorisation key to access source

    Returns:
        ManifestModel-like structure
    """

    manifest = {
        "sources": [
            {
                "type": type,
                "url": url,
                # id is basically MD5 hash digest of the URL
                "id": hashlib.md5(url.encode("utf-8")).hexdigest(),
                "pipeline": "pipeline",
            }
        ]
    }

    if api_key is not None:
        for source in manifest.get("sources"):
            source["credential"] = {
                "type": "headers",
                "content": {"Authorization": f"Bearer {api_key}"},
            }

    return manifest


def structure_transform_source_pipeline(
    source: str, transformer: str, arguments: Dict = {}
) -> PipelineModel:
    """
    Packs content according to List[PipelineModel] as required by the ingestion service for
    converting a source to a document.

    Arguments:
        source: source type
        transformer: source transformer name
        arguments: arguments (if any); defaults to {}

    Returns:
        PipelineModel
    """

    return {
        "name": "pipeline",
        "pipes": [
            {
                "name": "pipe",
                "sources": [{"source": source, "type": "manifest"}],
                "steps": [
                    {
                        "type": transformer,
                        "arguments": arguments,
                    },
                ],
            }
        ],
    }


def structure_transform_document_pipeline(
    transformer: str, arguments: Dict = {}
) -> PipelineModel:
    """
    Packs content according to List[PipelineModel] as required by the ingestion service for running
    the transformer on a document.

    Arguments:
        transformer: source transformer name
        arguments: arguments (if any); defaults to {}

    Returns:
        PipelineModel
    """

    return {
        "name": "pipeline",
        "pipes": [
            {
                "name": "pipe",
                "sources": [{"source": "document", "type": "manifest"}],
                "steps": [
                    # first step is to convert from document source type to document type
                    {
                        "type": "document_source_create_document",
                        "arguments": {},
                    },
                    {
                        "type": transformer,
                        "arguments": arguments,
                    },
                ],
            }
        ],
    }


async def ingest_envelope(envelope: EnvelopeModel) -> Dict:
    """
    Performs ingestion of an envelope. Sends a POST request to the ingestion service with the
    envelope, and returns the response.

    Arguments:
        envelope: a EnvelopeModel dict representing the envelope

    Returns:
        Ingestion response
    """

    headers = {"api_key": settings.INGESTOR_INGESTION_HOST_API_KEY}

    url = urljoin(settings.INGESTOR_INGESTION_HOST, "/api/v1/ingest/")

    response = await requests.post(
        url, json.dumps(jsonable_encoder(envelope)), headers=headers
    )

    return response

import os
from os.path import isfile, join
from typing import List

import aiofiles
from fastapi import APIRouter, HTTPException, File, status, UploadFile
from starlette.responses import FileResponse, JSONResponse

from ....config import get_settings


# fetch configuration settings
settings = get_settings()

# router for file operations
router = APIRouter()


@router.get("/")
def files() -> List[str]:
    """
    Returns list of user files' filenames in the workspace.

    Arguments:
        None

    Returns:
        List of filenames of user files in workspace.
    """

    return [
        f
        for f in os.listdir(settings.INGESTOR_WORKSPACE_DIR)
        if isfile(join(settings.INGESTOR_WORKSPACE_DIR, f))
    ]


@router.get("/file/{filename}", response_class=FileResponse)
def file(filename: str):
    """
    Returns the user file for a given file name.

    Arguments:
        filename - file name of file

    Returns:
        Corresponding user file
    """

    return FileResponse(join(settings.INGESTOR_WORKSPACE_DIR, filename))


@router.get("/samples")
def samples() -> List[str]:
    """
    Returns list of sample files' filenames in the workspace.

    Arguments:
        None

    Returns:
        List of filenames of sample files in workspace.
    """

    samples_dir = join(settings.INGESTOR_WORKSPACE_DIR, "samples")
    return [f for f in os.listdir(samples_dir) if isfile(join(samples_dir, f))]


@router.get("/sample/{filename}", response_class=FileResponse)
def sample(filename: str):
    """
    Returns the sample file for a given file name.

    Arguments:
        filename - file name of file

    Returns:
        Corresponding sample file
    """

    return FileResponse(join(join(settings.INGESTOR_WORKSPACE_DIR, "samples"), filename))


@router.post("/upload", response_class=JSONResponse)
async def upload(file: UploadFile = File(...)):
    """
    Uploads a file and store it on disk (workspace) with its file name.

    Arguments:
        file - uploaded file

    Returns:
        file name as a JSON response
    """

    async with aiofiles.open(
        join(settings.INGESTOR_WORKSPACE_DIR, file.filename), "wb"
    ) as out_file:
        content = await file.read()
        await out_file.write(content)

        """ prefer using chunks, but some problem with walrus operator """
        # in 1024 byte chunks
        # while content := await file.read(1024)
        #     await out_file.write(content)

    return JSONResponse({"filename": file.filename})


@router.delete("/delete/{filename}", status_code=status.HTTP_200_OK)
def delete(filename: str) -> None:
    """
    Deletes a user file from the workspace.

    Arguments:
        filename: file name of file to delete

    Returns:
        None
    """

    try:
        os.remove(join(settings.INGESTOR_WORKSPACE_DIR, filename))
    except:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="File not found"
        )

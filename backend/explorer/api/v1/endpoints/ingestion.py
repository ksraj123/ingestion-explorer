import json
from os.path import join
from typing import Dict
from urllib.parse import urljoin

from fastapi import APIRouter, HTTPException, Request, status
from fastapi.encoders import jsonable_encoder
import requests_async as requests
from starlette.responses import JSONResponse

from ....config import get_settings
from .models.ingestion import (
    SourceTransformerPackageModel,
    DocumentTransformerPackageModel,
    EnvelopeModel,
)
from .utils.schema import format_schema
from .utils.ingestion import (
    fetch_schema,
    make_file,
    ingest_envelope,
    remove_file,
    structure_manifest,
    structure_transform_source_pipeline,
    structure_transform_document_pipeline,
)


# fetch configuration settings
settings = get_settings()

# router for ingestion operations
router = APIRouter()


@router.get("/schema", response_class=JSONResponse)
async def schema() -> Dict:
    """
    Returns the schema of the sources, documents, manifest sources, conditionals, and transformers.

    Arguments:
        None

    Returns:
        The JSON data structure containing the schema
    """

    response = await fetch_schema()
    if response.status_code == status.HTTP_200_OK:
        schema = response.json().get("components").get("schemas")
        return JSONResponse(format_schema(schema))

    raise HTTPException(
        status_code=response.status_code, detail=response.json().get("detail")
    )


@router.post("/transform-source", response_class=JSONResponse)
async def transform_source(package: SourceTransformerPackageModel, request: Request):
    """
    Takes the package for a source transformer, converts it into ingestion service envelope format, sends a POST request to the ingestion service, and returns the document from the response.

    Arguments:
        package: data inputs for running the transformer

    Returns:
        Ingested document result or error as a JSON response
    """

    api_key = request.headers.get("api_key")

    url = package.source
    if package.origin_type == "FILE" or package.origin_type == "SAMPLE":
        url = urljoin(
            settings.INGESTOR_APP_PUBLIC_URL,
            join(
                settings.INGESTOR_API_TREE,
                f"files/{'file' if package.origin_type == 'FILE' else 'sample'}/{package.source}",
            )[1:],
        )

    manifest = structure_manifest(package.source_type, url, api_key)
    pipelines = [
        structure_transform_source_pipeline(
            package.source_type, package.transformer, package.arguments
        )
    ]
    envelope: EnvelopeModel = {"manifest": manifest, "pipelines": pipelines}

    response = await ingest_envelope(envelope)
    if response.status_code == status.HTTP_200_OK:
        content = response.json()
        if "results" in content and content.get("results") is not None:
            content = content.get("results")[0].get("document")
        return JSONResponse(content=content)

    raise HTTPException(
        status_code=response.status_code, detail=response.json().get("detail")
    )


@router.post("/transform-document", response_class=JSONResponse)
async def transform_document(
    package: DocumentTransformerPackageModel, request: Request
):
    """
    Takes the package for a transformer, converts it into ingestion service envelope format, sends a
    POST request to the ingestion service, and returns the document from the response.

    Arguments:
        package: data inputs for running the transformer

    Returns:
        Ingested document result or error as a JSON response
    """

    api_key = request.headers.get("api_key")

    """
    Make a temporary JSON file and dump the contents of the document format into it. This will be
    used by the ingestion service. The ingestion service envelope is then created and sent to the
    ingestion service as a POST request. Remove the temporary file, and return the document from the
    received response.
    """
    make_file(
        join(settings.INGESTOR_WORKSPACE_DIR, f"{api_key}.json"),
        json.dumps(jsonable_encoder(package.document)),
    )

    manifest = structure_manifest(
        "document",
        urljoin(
            settings.INGESTOR_APP_PUBLIC_URL,
            join(settings.INGESTOR_API_TREE, f"files/file/{api_key}.json")[1:],
        ),
        api_key,
    )
    pipelines = [
        structure_transform_document_pipeline(package.transformer, package.arguments)
    ]
    envelope: EnvelopeModel = {"manifest": manifest, "pipelines": pipelines}

    response = await ingest_envelope(envelope)

    # removal could fail, although not likely
    try:
        remove_file(join(settings.INGESTOR_WORKSPACE_DIR, f"{api_key}.json"))
    except:
        pass

    if response.status_code == status.HTTP_200_OK:
        content = response.json()
        if "results" in content and content.get("results") is not None:
            content = content.get("results")[0].get("document")
        return JSONResponse(content=content)

    raise HTTPException(
        status_code=response.status_code, detail=response.json().get("detail")
    )


@router.post("/ingest", response_class=JSONResponse)
async def ingest(envelope: EnvelopeModel):
    """
    Sends the ingestion service format envelope as a POST request to the ingestion service, and
    returns the document from the response.

    Arguments:
        package: data inputs for running the transformer

    Returns:
        Ingested document result as a JSON response

    Exception:
        HTTPException object in case of failure
    """

    headers = {"api_key": settings.INGESTOR_INGESTION_HOST_API_KEY}

    url = urljoin(settings.INGESTOR_INGESTION_HOST, "/sync/api/v1/ingest/")
    response = await requests.post(
        url, json.dumps(jsonable_encoder(envelope)), headers=headers
    )

    # Return ingestion output document from the response only when there is a useful response :p
    if response.status_code == 200:
        content = response.json()
        if "results" in content:
            return JSONResponse(
                content=response.json().get("results")[0].get("document")
            )
        else:
            return JSONResponse(content=content)

    raise HTTPException(status_code=response.status_code)

from pydantic import BaseModel
from typing import Any, Dict, List, Optional


class SourceTransformerPackageModel(BaseModel):
    """Request model the source transform endpoint."""

    source_type: str
    origin_type: str
    source: str
    transformer: str
    arguments: Dict = None


class DocumentModel(BaseModel):
    """Model representing an Ingestum document."""

    type: str
    title: str = ""
    content: str
    context: Dict = {}
    origin: Any
    version: str = "1.0"
    pdf_context: Any


class DocumentTransformerPackageModel(BaseModel):
    """Request model for the document transform endpoint."""

    transformer: str
    arguments: Dict
    document: DocumentModel


class ManifestSourceCredential(BaseModel):
    """Model for manifest source credential"""

    type: str
    content: Dict


class ManifestSourceModel(BaseModel):
    """Model for each manifest source."""

    type: str
    url: str
    id: str
    pipeline: str
    credential: Optional[ManifestSourceCredential]


class ManifestModel(BaseModel):
    """Model for each manifest."""

    sources: List[ManifestSourceModel]


class PipeSourceModel(BaseModel):
    """Model for each pipe source."""

    source: str
    type: str


class PipeStepModel(BaseModel):
    """Model for each pipe step (transformer)."""

    type: str
    arguments: Dict


class PipeModel(BaseModel):
    """Model for each pipe."""

    name: str
    sources: List[PipeSourceModel]
    steps: List[PipeStepModel]


class PipelineModel(BaseModel):
    """Model for each pipeline."""

    name: str
    pipes: List[PipeModel]


class EnvelopeModel(BaseModel):
    """Request model for the ingestion service."""

    manifest: ManifestModel
    pipelines: List[PipelineModel]

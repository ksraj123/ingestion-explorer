#!/bin/bash
set -euxo pipefail

source $(dirname "$0")/vars

export BUILD_ON_DEMAND=0
if [ -z ${COMMIT_SHA+x} ] || [[ "$COMMIT_SHA" == "HEAD" ]]; then
    export COMMIT_SHA="$(git rev-parse --short HEAD)"
    echo "COMMIT_SHA is unset fallback, to $COMMIT_SHA"
elif [[ "$COMMIT_SHA" != "$(git rev-parse --short HEAD)" ]]; then
    # the user has provided a custom image hash
    # which we will need to deploy
    export BUILD_ON_DEMAND=1
    echo "Building $COMMIT_SHA on demand"
    git checkout "$COMMIT_SHA"
    export COMMIT_SHA="$(git rev-parse --short $COMMIT_SHA)"
fi

echo "========== BUILDING CONTAINERS ============"
echo "Building on branch: '$BRANCH_NAME'"

gcloud builds submit \
  --config=scripts/gcp/cicd/cloudbuild.yaml \
  --substitutions=_COMMIT_SHA=${COMMIT_SHA},_GCR_PROJECT_ID=${GCR_PROJECT_ID} \
  --timeout=3h

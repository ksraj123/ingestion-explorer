#!/bin/bash
set -euxo pipefail

source $(dirname "$0")/vars


if [ -z ${COMMIT_SHA+x} ] || [[ "$COMMIT_SHA" == "HEAD" ]]; then
    export COMMIT_SHA="$(git rev-parse --short HEAD)"
    echo "COMMIT_SHA is unset fallback, to $COMMIT_SHA"
elif [[ "$COMMIT_SHA" != "$(git rev-parse --short HEAD)" ]]; then
    # the user has provided a custom image hash
    # which we will need to deploy
    git checkout "$COMMIT_SHA"
    export COMMIT_SHA="$(git rev-parse --short $COMMIT_SHA)"
fi

# checks if all the containers are present in the google container registry
gcloud container images list-tags \
  "$GCR_PREFIX/$GCR_PROJECT_ID/$DOCKER_CONTAINER_NAME_PREFIX-frontend" | grep "$(git rev-parse --short HEAD)"
gcloud container images list-tags \
  "$GCR_PREFIX/$GCR_PROJECT_ID/$DOCKER_CONTAINER_NAME_PREFIX-backend" | grep "$(git rev-parse --short HEAD)"
